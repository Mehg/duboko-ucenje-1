# Duboko učenje  1

0 - [Logistička regresija, gradijentni spust, Python, numpy](http://www.zemris.fer.hr/~ssegvic/du/lab0.shtml)

1 - [Osnove dubokih modela, PyTorch, MNIST](http://www.zemris.fer.hr/~ssegvic/du/lab1.shtml)

2 - [Konvolucijski modeli, MNIST, CIFAR](https://dlunizg.github.io/lab2/)

3 - [Povratni modeli](https://dlunizg.github.io/lab3/)

4 - [Metrička ugrađivanja](https://dlunizg.github.io/lab4_metric/)

