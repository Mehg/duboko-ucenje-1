import torch
import numpy as np
from torch import nn
from pathlib import Path
from torch import optim
import os
import math
import skimage as ski
from torchvision.datasets import MNIST
import skimage.io

DATA_DIR = Path(__file__).parent / 'datasets' / 'MNIST'
SAVE_DIR = Path(__file__).parent / 'out' / '3_pytorch_conv' / '0.1'

config = {}
max_epochs = 8
batch_size = 50
weight_decay = 1e-1
lr_policy = {1: 1e-1, 3: 1e-2, 5: 1e-3, 7: 1e-4}


def dense_to_one_hot(y, class_count):
    return np.eye(class_count)[y]


class CovolutionalModel(nn.Module):
    def __init__(self):
        super().__init__()
        self.conv1 = nn.Conv2d(1, 16, kernel_size=5, stride=1, padding=2, bias=True)
        self.pool1 = nn.MaxPool2d(kernel_size=2, stride=2)
        self.conv2 = nn.Conv2d(16, 32, kernel_size=5, stride=1, padding=2, bias=True)
        self.pool2 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.fc1 = nn.Linear(32 * 7 * 7, 512, bias=True)
        self.fc2 = nn.Linear(512, 10, bias=True)

        self.reset_parameters()

    def reset_parameters(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight, mode='fan_in', nonlinearity='relu')
                nn.init.constant_(m.bias, 0)
            elif isinstance(m, nn.Linear) and m is not self.fc2:
                nn.init.kaiming_normal_(m.weight, mode='fan_in', nonlinearity='relu')
                nn.init.constant_(m.bias, 0)
        self.fc2.reset_parameters()

    def forward(self, x):
        h = self.conv1(x)
        h = self.pool1(h)
        h = torch.relu(h)

        h = self.conv2(h)
        h = self.pool2(h)
        h = torch.relu(h)

        h = h.view((h.shape[0], -1))

        h = self.fc1(h)
        h = torch.relu(h)
        h = self.fc2(h)
        return h

    def loss(self, x, y):
        log = torch.log(torch.sum(torch.exp(x), dim=1))
        sum_xy = torch.sum(x * y, dim=1)
        loss = torch.mean(log - sum_xy)
        return loss

    def trainin(self, train_x, train_y, valid_x, valid_y):
        losses = []
        num_examples = train_x.shape[0]
        num_batches = num_examples // batch_size
        optimizer = optim.SGD(params=self.parameters(), weight_decay=weight_decay, lr=lr_policy[1])

        for epoch in range(max_epochs):
            losses_for_one = []

            if epoch in lr_policy:
                for param_group in optimizer.param_groups:
                    param_group['lr'] = lr_policy[epoch]

            permutation_idx = np.random.permutation(num_examples)
            x = train_x[permutation_idx]
            x = torch.tensor(x).float()
            y = train_y[permutation_idx]
            y = torch.tensor(y)

            cnt_correct = 0
            for i in range(num_batches):
                batch_x = x[i * batch_size:(i + 1) * batch_size, :]
                batch_y = y[i * batch_size:(i + 1) * batch_size, :]

                logits = self.forward(batch_x)
                loss = self.loss(logits, batch_y)
                losses_for_one.append(loss.item())
                loss.backward()

                optimizer.step()
                optimizer.zero_grad()

                yp = torch.argmax(logits, 1)
                yt = torch.argmax(batch_y, 1)
                cnt_correct += (yp == yt).sum().detach().numpy()

                if i % 5 == 0:
                    print("epoch %d, step %d/%d, batch loss = %.2f" % (epoch, i * batch_size, num_examples, loss.item()))

                if i % 100 == 0:
                    draw_filters(epoch, i * batch_size, self.conv1)

                if i > 0 and i % 50 == 0:
                    print("Train accuracy = %.2f" % (cnt_correct / ((i + 1) * batch_size) * 100))

            losses.append(np.mean(losses_for_one))

        self.evaluate("Train", torch.tensor(train_x).float(), torch.tensor(train_y))
        self.evaluate("Validation", torch.tensor(valid_x).float(), torch.tensor(valid_y))

        with open(SAVE_DIR / 'losses.txt', 'w') as f:
            for item in losses:
                f.write("%s\n" % item)

    def evalu(self, X):
        return self.forward(X).clone()

    def evaluate(self, name, x, y):
        print("\nRunning evaluation: ", name)
        num_examples = x.shape[0]
        num_batches = num_examples // batch_size

        cnt_correct = 0
        loss_avg = 0

        for i in range(num_batches):
            batch_x = x[i * batch_size:(i + 1) * batch_size, :]
            batch_y = y[i * batch_size:(i + 1) * batch_size, :]
            logits = self.evalu(batch_x)
            yp = torch.argmax(logits, 1)
            yt = torch.argmax(batch_y, 1)
            cnt_correct += (yp == yt).sum().detach().numpy()
            loss_val = self.loss(logits, batch_y).detach().numpy()
            loss_avg += loss_val

        valid_acc = cnt_correct / num_examples * 100
        loss_avg /= num_batches
        print(name + " accuracy = %.2f" % valid_acc)
        print(name + " avg loss = %.2f\n" % loss_avg)


def draw_filters(epoch, step, layer):
    border = 1
    cols = 8

    w = layer.weight.clone().detach().numpy()
    num_filters, C, w1, h1 = w.shape[:4]

    w -= w.min()
    w /= w.max()

    rows = math.ceil(num_filters / cols)
    width = cols * w1 + (cols - 1) * border
    height = rows * h1 + (rows - 1) * border

    for i in range(1):
        img = np.zeros([height, width])
        for j in range(num_filters):
            r = int(j / cols) * (w1 + border)
            c = int(j % cols) * (h1 + border)
            img[r:r + w1, c:c + h1] = w[j, i]
        filename = 'conv1_epoch_%02d_step_%06d_input_%03d.png' % (epoch, step, i)
        ski.io.imsave(os.path.join(SAVE_DIR, filename), np.array(img*255, dtype=np.uint8))


ds_train, ds_test = MNIST(DATA_DIR, train=True, download=True), MNIST(DATA_DIR, train=False)
train_x = ds_train.data.reshape([-1, 1, 28, 28]).numpy().astype(float) / 255
train_y = ds_train.targets.numpy()
train_x, valid_x = train_x[:55000], train_x[55000:]
train_y, valid_y = train_y[:55000], train_y[55000:]
test_x = ds_test.data.reshape([-1, 1, 28, 28]).numpy().astype(float) / 255
test_y = ds_test.targets.numpy()
train_mean = train_x.mean()
train_x, valid_x, test_x = (x - train_mean for x in (train_x, valid_x, test_x))
train_y, valid_y, test_y = (dense_to_one_hot(y, 10) for y in (train_y, valid_y, test_y))

model = CovolutionalModel()
model.trainin(train_x, train_y, valid_x, valid_y)

model.evaluate("Test", torch.tensor(test_x).float(), torch.tensor(test_y))
