import os
from sklearn.metrics import confusion_matrix
import pickle
import numpy as np
import torch
from torch import nn
from pathlib import Path
from torch import optim
import os
import math
import skimage as ski
import skimage.io

CIFAR_DIR = Path(__file__).parent / 'datasets' / 'CIFAR'
DATA_DIR = Path(__file__).parent / 'datasets' / 'CIFAR' / 'cifar-10-batches-py'
SAVE_DIR = Path(__file__).parent / 'out' / '4_cifar'
WORST_DIR = Path(__file__).parent / 'out' / '4_cifar' / 'worst'

max_epochs = 8
batch_size = 50
weight_decay = 1e-4
lr = 1e-2
classes = 10


def dense_to_one_hot(y, class_count):
    return np.eye(class_count)[y]


def shuffle_data(data_x, data_y):
    indices = np.arange(data_x.shape[0])
    np.random.shuffle(indices)
    shuffled_data_x = np.ascontiguousarray(data_x[indices])
    shuffled_data_y = np.ascontiguousarray(data_y[indices])
    return shuffled_data_x, shuffled_data_y


def unpickle(file):
    fo = open(file, 'rb')
    dict = pickle.load(fo, encoding='latin1')
    fo.close()
    return dict


def init_dataset():
    img_height = 32
    img_width = 32
    num_channels = 3

    train_x = np.ndarray((0, img_height * img_width * num_channels), dtype=np.float32)
    train_y = []
    for i in range(1, 6):
        subset = unpickle(os.path.join(DATA_DIR, 'data_batch_%d' % i))
        train_x = np.vstack((train_x, subset['data']))
        train_y += subset['labels']
    train_x = train_x.reshape((-1, num_channels, img_height, img_width)).transpose(0, 2, 3, 1)
    train_y = np.array(train_y, dtype=np.int32)

    subset = unpickle(os.path.join(DATA_DIR, 'test_batch'))
    test_x = subset['data'].reshape((-1, num_channels, img_height, img_width)).transpose(0, 2, 3, 1).astype(np.float32)
    test_y = np.array(subset['labels'], dtype=np.int32)

    valid_size = 5000
    train_x, train_y = shuffle_data(train_x, train_y)
    valid_x = train_x[:valid_size, ...]
    valid_y = train_y[:valid_size, ...]
    train_x = train_x[valid_size:, ...]
    train_y = train_y[valid_size:, ...]
    data_mean = train_x.mean((0, 1, 2))
    data_std = train_x.std((0, 1, 2))

    train_x = (train_x - data_mean) / data_std
    valid_x = (valid_x - data_mean) / data_std
    test_x = (test_x - data_mean) / data_std

    train_x = train_x.transpose(0, 3, 1, 2)
    valid_x = valid_x.transpose(0, 3, 1, 2)
    test_x = test_x.transpose(0, 3, 1, 2)

    return train_x, train_y, valid_x, valid_y, test_x, test_y, data_mean, data_std


class CovolutionalModel2(nn.Module):
    def __init__(self):
        super().__init__()
        self.conv1 = nn.Conv2d(3, 16, kernel_size=5, stride=1, padding=2, bias=True)
        self.pool1 = nn.MaxPool2d(kernel_size=3, stride=2)
        self.conv2 = nn.Conv2d(16, 32, kernel_size=5, stride=1, padding=2, bias=True)
        self.pool2 = nn.MaxPool2d(kernel_size=3, stride=2)

        self.fc1 = nn.Linear(32 * 7 * 7, 256, bias=True)
        self.fc2 = nn.Linear(256, 128, bias=True)
        self.fc3 = nn.Linear(128, classes, bias=True)

        self.reset_parameters()

    def reset_parameters(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight, mode='fan_in', nonlinearity='relu')
                nn.init.constant_(m.bias, 0)
            elif isinstance(m, nn.Linear) and m is not self.fc3:
                nn.init.kaiming_normal_(m.weight, mode='fan_in', nonlinearity='relu')
                nn.init.constant_(m.bias, 0)
        self.fc3.reset_parameters()

    def forward(self, x):
        h = self.conv1(x)
        h = torch.relu(h)
        h = self.pool1(h)

        h = self.conv2(h)
        h = torch.relu(h)
        h = self.pool2(h)

        h = h.view((h.shape[0], -1))

        h = self.fc1(h)
        h = torch.relu(h)
        h = self.fc2(h)
        h = torch.relu(h)
        h = self.fc3(h)
        return h

    def loss(self, y_pred, y_true):
        log = torch.log(torch.sum(torch.exp(y_pred), dim=1))
        sum_xy = torch.sum(y_pred * y_true, dim=1)
        loss = torch.mean(log - sum_xy)  # myb -loss TODO
        return loss

    def loss_for_one(self, y_pred, y_true):
        log = torch.log(torch.sum(torch.exp(y_pred)))
        sum_xy = torch.sum(y_pred * y_true)
        loss = log - sum_xy  # myb -loss TODO
        return loss

    def trainin(self, train_x, train_y, valid_x, valid_y):
        losses_train = []
        losses_valid = []
        learning_rates = []
        accuracies_train = []
        accuracies_valid = []

        num_examples = train_x.shape[0]
        num_batches = num_examples // batch_size
        optimizer = optim.SGD(params=self.parameters(), weight_decay=weight_decay, lr=lr)
        scheduler = optim.lr_scheduler.ExponentialLR(optimizer, gamma=0.9)

        # prosječnu vrijednost funkcije gubitka, stopu učenja te ukupnu točnost klasifikacije

        for epoch in range(max_epochs):

            permutation_idx = np.random.permutation(num_examples)
            x = train_x[permutation_idx]
            x = torch.tensor(x).float()
            y = train_y[permutation_idx]
            y = torch.tensor(y)

            cnt_correct = 0
            for i in range(num_batches):
                batch_x = x[i * batch_size:(i + 1) * batch_size, :]
                batch_y = y[i * batch_size:(i + 1) * batch_size, :]

                logits = self.forward(batch_x)
                loss = self.loss(logits, batch_y)
                loss.backward()

                optimizer.step()
                optimizer.zero_grad()

                yp = torch.argmax(logits, 1)
                yt = torch.argmax(batch_y, 1)
                cnt_correct += (yp == yt).sum().detach().numpy()

                if i % 5 == 0:
                    print(
                        "epoch %d, step %d/%d, batch loss = %.2f" % (
                            epoch + 1, i * batch_size, num_examples, loss.item()))

                if i % 100 == 0:
                    draw_conv_filters(epoch + 1, i * batch_size, self.conv1.weight.clone().detach().numpy())

                if i > 0 and i % 50 == 0:
                    print("Train accuracy = %.2f" % (cnt_correct / ((i + 1) * batch_size) * 100))

            learning_rates.append(optimizer.param_groups[0]['lr'])
            scheduler.step()

            with torch.no_grad():
                train_eval = self.evaluate("Train", train_x, train_y)
                val_eval = self.evaluate("Validation", valid_x, valid_y)

                losses_train.append(train_eval[0])
                losses_valid.append(val_eval[0])
                accuracies_train.append(train_eval[1])
                accuracies_valid.append(val_eval[1])

        with open(SAVE_DIR / 'losses_train.txt', 'w') as f:
            for item in losses_train:
                f.write("%s\n" % item)

        with open(SAVE_DIR / 'losses_valid.txt', 'w') as f:
            for item in losses_valid:
                f.write("%s\n" % item)

        with open(SAVE_DIR / 'learning_rates.txt', 'w') as f:
            for item in learning_rates:
                f.write("%s\n" % item)

        with open(SAVE_DIR / 'accuracies_train.txt', 'w') as f:
            for item in accuracies_train:
                f.write("%s\n" % item)

        with open(SAVE_DIR / 'accuracies_valid.txt', 'w') as f:
            for item in accuracies_valid:
                f.write("%s\n" % item)

    def evaluate(self, name, x, y):
        print("\nRunning evaluation: ", name)
        num_examples = x.shape[0]
        num_batches = num_examples // batch_size

        x = torch.tensor(x)
        y = torch.tensor(y)

        y_true = []
        y_pred = []
        losses = []

        for i in range(num_batches):
            batch_x = x[i * batch_size:(i + 1) * batch_size, :]
            batch_y = y[i * batch_size:(i + 1) * batch_size, :]

            logits = self.forward(batch_x)
            loss = self.loss(logits, batch_y)
            losses.append(loss.item())

            yp = torch.argmax(logits, 1)
            yt = torch.argmax(batch_y, 1)

            y_true.extend(yt)
            y_pred.extend(yp)

        loss_avg = np.mean(losses)

        matrix = confusion_matrix(y_true, y_pred)

        acc = np.trace(matrix) / num_examples
        precisions = []
        recalls = []

        for i in range(classes):
            tp = matrix[i][i]
            fp = np.sum(matrix[:, i]) - tp
            fn = np.sum(matrix[i, :]) - tp
            p = tp / (tp + fp)
            r = tp / (tp + fn)
            precisions.append(p)
            recalls.append(r)

        print(name + " accuracy = %.2f" % (acc * 100))
        print(name + " avg loss = %.2f\n" % loss_avg)
        print(name + " confusion matrix \n " + str(matrix))
        print(name + " precisions = " + str(precisions))
        print(name + " recalls f= " + str(recalls))

        return loss_avg, acc

    def find_worst(self, x, y, mean, std):
        num_examples = x.shape[0]
        num_batches = num_examples // batch_size

        x = torch.tensor(x)
        y = torch.tensor(y)

        worst = []
        for i in range(num_batches):
            batch_x = x[i * batch_size:(i + 1) * batch_size, :]
            batch_y = y[i * batch_size:(i + 1) * batch_size, :]

            logits = self.forward(batch_x)

            yp = torch.argmax(logits, 1)
            yt = torch.argmax(batch_y, 1)

            batch_x = batch_x[yt != yp]
            batch_y = batch_y[yt != yp]
            logits = logits[yt != yp]

            worst.extend([[x.detach().numpy(), self.loss_for_one(logits[idx], batch_y[idx]).item(), logits[idx].detach().numpy()] for idx, x in enumerate(batch_x)])

        worst.sort(key=lambda x: x[1])
        worst = np.array(worst, dtype=object)
        worst = worst[:20]

        predictions = worst[:, 2]
        predictions = [np.argpartition(pred, -3)[-3:] for pred in predictions]
        worst = worst[:, 0]

        for i, w in enumerate(worst):
            img = w.transpose(1, 2, 0)
            img *= std
            img += mean
            ski.io.imsave(os.path.join(WORST_DIR, f'{i}.png'), np.array(img, dtype=np.uint8))

        with open(WORST_DIR / 'worst_predictions.txt', 'w') as f:
            for item in predictions:
                f.write("%s\n" % item[0])
                f.write("%s\n" % item[1])
                f.write("%s\n" % item[2])


def draw_conv_filters(epoch, step, weights):
    w = weights.copy()
    num_filters = w.shape[0]
    num_channels = w.shape[1]
    k = w.shape[2]
    assert w.shape[3] == w.shape[2]
    w = w.transpose(2, 3, 1, 0)
    w -= w.min()
    w /= w.max()
    border = 1
    cols = 8
    rows = math.ceil(num_filters / cols)
    width = cols * k + (cols - 1) * border
    height = rows * k + (rows - 1) * border
    img = np.zeros([height, width, num_channels])
    for i in range(num_filters):
        r = int(i / cols) * (k + border)
        c = int(i % cols) * (k + border)
        img[r:r + k, c:c + k, :] = w[:, :, :, i]
    filename = 'epoch_%02d_step_%06d.png' % (epoch, step)
    ski.io.imsave(os.path.join(SAVE_DIR, filename), np.array(img * 255, dtype=np.uint8))


train_x, train_y, valid_x, valid_y, test_x, test_y, mean, std = init_dataset()
train_y, valid_y, test_y = (dense_to_one_hot(y, classes) for y in (train_y, valid_y, test_y))

model = CovolutionalModel2()
model.trainin(train_x, train_y, valid_x, valid_y)
model.find_worst(train_x, train_y, mean, std)
# model.evaluate("Test", test_x, test_y)

# zao mi je za tezak labos
# is luw u