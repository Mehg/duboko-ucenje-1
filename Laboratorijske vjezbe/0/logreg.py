import numpy as np
import data

param_niter = 1000
param_delta = 0.01


def logreg_train(X, Y_):
    N = len(X)
    C = max(Y_) + 1
    D = len(X[0])
    W = np.random.randn(D, C)
    b = np.zeros((1, C))

    # gradijentni spust (param_niter iteracija)
    for i in range(param_niter):
        # eksponencirane klasifikacijske mjere
        # pri računanju softmaksa obratite pažnju
        # na odjeljak 4.1 udžbenika
        # (Deep Learning, Goodfellow et al)!
        scores = np.dot(X, W) + b  # N x C
        expscores = np.exp(scores)  # N x C

        # nazivnik sofmaksa
        sumexp = np.sum(expscores, axis=1).reshape((N, 1))  # N x 1

        # logaritmirane vjerojatnosti razreda
        probs = expscores / sumexp  # N x C
        logprobs = np.log(probs)  # N x C

        # gubitak
        loss = -1 / len(logprobs) * np.sum(logprobs)  # scalar

        # dijagnostički ispis
        if i % 10 == 0:
            print("iteration {}: loss {}".format(i, loss))

        # derivacije komponenata gubitka po mjerama
            dL_ds = probs  # N x C

        for i in range(len(Y_)):
            dL_ds[i][Y_[i]] -= 1

        # gradijenti parametara
        grad_W = 1 / len(X) * np.matmul(np.transpose(X), dL_ds)  # C x D (ili D x C)
        grad_b = 1 / len(X) * np.sum(dL_ds, axis=0)  # C x 1 (ili 1 x C)
        grad_b.reshape((1, C))
        # poboljšani parametri
        W += -param_delta * grad_W
        b += -param_delta * grad_b

    return W, b


def logreg_classify(X, W, b):
    probs = np.matmul(X, W) + b
    probs = np.e ** probs / np.sum(np.e ** probs, axis=0)
    return probs


def logreg_decfun(W, b):
    def classify(X):
        probs = logreg_classify(X, w, b)
        Y = np.array([np.argmax(row) for row in probs])

        return Y

    return classify


if __name__ == "__main__":
    np.random.seed(100)

    # get the training dataset
    X, Y_ = data.sample_gauss_2d(3, 100)

    # train the model
    w, b = logreg_train(X, Y_)

    # evaluate the model on the training dataset
    probs = logreg_classify(X, w, b)
    Y = np.array([np.argmax(row) for row in probs])

    # report performance
    accuracy, pr, M = data.eval_perf_multi(Y, Y_)

    print(accuracy, pr, M)

    # graph the decision surface
    decfun = logreg_decfun(w, b)
    bbox = (np.min(X, axis=0), np.max(X, axis=0))
    data.graph_surface(decfun, bbox, offset=0.5)

    # graph the data points
    data.graph_data(X, Y_, Y, special=[])
