import numpy as np
import data

param_niter = 1000
param_delta = 0.01


def binlogreg_train(X, Y_):
    '''
        Argumenti
          X:  podatci, np.array NxD
          Y_: indeksi razreda, np.array Nx1

        Povratne vrijednosti
          w, b: parametri logističke regresije
      '''
    w = np.random.randn(len(X[0]), 1)
    b = 0
    # gradijentni spust (param_niter iteracija)
    for i in range(param_niter):
        # klasifikacijske mjere
        scores = np.dot(X, w) + b
        # vjerojatnosti razreda c_1
        probs = np.e ** scores / (np.e ** scores + 1)

        # gubitak
        loss = 1 / len(X) * np.sum(np.log(probs))

        # dijagnostički ispis
        if i % 10 == 0:
            print("iteration {}: loss {}".format(i, loss))

        # derivacije gubitka po klasifikacijskim mjerama
        dL_dscores = np.subtract(probs, Y_.reshape(probs.shape))  # N x 1

        # gradijenti parametara
        grad_w = 1 / len(X) * np.dot(np.transpose(X), dL_dscores)  # D x 1
        grad_b = 1 / len(X) * np.sum(dL_dscores)  # 1 x 1

        # poboljšani parametri
        w += -param_delta * grad_w
        b += -param_delta * grad_b

    return w, b


def binlogreg_classify(X, w, b):
    '''
      Argumenti
          X:    podatci, np.array NxD
          w, b: parametri logističke regresije

      Povratne vrijednosti
          probs: vjerojatnosti razreda c1
    '''
    probs = np.dot(X, w) + b
    probs = np.e ** probs / (1 + np.e ** probs)
    return probs


def binlogreg_decfun(w, b):
    def classify(X):
        return binlogreg_classify(X, w, b)

    return classify


if __name__ == "__main__":
    np.random.seed(100)

    # get the training dataset
    X, Y_ = data.sample_gauss_2d(2, 100)

    # train the model
    w, b = binlogreg_train(X, Y_)

    # evaluate the model on the training dataset
    probs = binlogreg_classify(X, w, b)
    Y = np.reshape(probs >= 0.5, Y_.shape)

    # report performance
    accuracy, recall, precision = data.eval_perf_binary(Y, Y_)
    AP = data.eval_AP(Y_[probs.argsort()])

    print(accuracy, recall, precision, AP)

    # graph the decision surface
    decfun = binlogreg_decfun(w, b)
    bbox = (np.min(X, axis=0), np.max(X, axis=0))
    data.graph_surface(decfun, bbox, offset=0.5)

    # graph the data points
    data.graph_data(X, Y_, Y, special=[])
