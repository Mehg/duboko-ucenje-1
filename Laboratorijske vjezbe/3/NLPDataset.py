from torch.utils.data import Dataset
from dataclasses import dataclass

import pandas as pd
import Vocab


class NLPDataset(Dataset):
    def __init__(self, dataset_file, text_vocab: Vocab, label_vocab: Vocab):
        instances = []  # turn into Instance class

        lines = pd.read_csv(dataset_file, header=None)
        for idx, line in lines.iterrows():
            text = line[0].split(" ")
            label = line[1].strip()
            instances.append(Instance(text, label))

        self.instances = instances
        self.text_vocab = text_vocab
        self.label_vocab = label_vocab

    def __len__(self):
        return len(self.instances)

    def __getitem__(self, idx):
        instance = self.instances[idx]
        return self.text_vocab.encode(instance.text), self.label_vocab.encode(instance.label)


@dataclass
class Instance:
    """Class for keeping track of an item in inventory."""
    text: list
    label: str

    def __init__(self, text: list, label: str):
        self.text = text
        self.label = label

    def __iter__(self):
        return iter((self.text, self.label))
