import torch.nn
from sklearn.metrics import confusion_matrix
import numpy as np


class RecurrentModel(torch.nn.Module):
    def __init__(self, embeddings, rnn_cell_type: torch.nn.Module, hidden_size: int, num_layers: int, dropout: float,
                 bidirectional: bool) -> None:
        super().__init__()
        self.embeddings = embeddings

        input_other = hidden_size * 2 if bidirectional else hidden_size
        self.rnn1 = rnn_cell_type(input_size=300,
                                  hidden_size=hidden_size,
                                  num_layers=num_layers,
                                  batch_first=False,
                                  dropout=dropout,
                                  bidirectional=bidirectional)
        self.rnn2 = rnn_cell_type(input_size=input_other,
                                  hidden_size=hidden_size,
                                  num_layers=num_layers,
                                  batch_first=False,
                                  dropout=dropout,
                                  bidirectional=bidirectional)

        self.fc1 = torch.nn.Linear(input_other, hidden_size)
        self.fc2 = torch.nn.Linear(hidden_size, 1)

    def forward(self, x):
        x = self.embeddings(x)
        x = torch.transpose(x, 0, 1)

        h = None
        x, h = self.rnn1(x, h)
        x, h = self.rnn2(x, h)

        # x = torch.nn.utils.rnn.pad_sequence(x, batch_first=False, padding_value=0.0)
        # x = torch.mean(x, dim=1)
        x = x[:, -1, :]
        x = torch.relu(self.fc1(x))
        x = self.fc2(x)

        return torch.reshape(x, (len(x),))
        # return x


def train(model, data, optimizer, criterion, clip):
    model.train()

    for batch_num, batch in enumerate(data):
        x = batch[0]
        y = batch[1]
        logits = model.forward(x)
        loss = criterion(logits, y.float())
        loss.backward()
        torch.nn.utils.clip_grad_norm_(model.parameters(), clip)
        optimizer.step()
        optimizer.zero_grad()


def evaluate(model, data, criterion):
    model.eval()

    y_true = []
    y_pred = []
    losses = []

    with torch.no_grad():
        for batch_num, batch in enumerate(data):
            x = batch[0]
            y = batch[1].float()
            logits = model(x)
            pred = [1 if logit >= 0.5 else 0 for logit in logits]
            y_true.extend(y)
            y_pred.extend(pred)
            loss = criterion(logits, y)
            losses.append(loss)

    y_true = np.array(y_true)
    y_pred = np.array(y_pred)
    cm = confusion_matrix(y_true, y_pred)
    tn, fp, fn, tp = cm.ravel()

    accuracy = (tp + tn) / (tp + tn + fp + fn)
    p = (tp) / (tp + fp)
    r = (tp) / (tp + fn)
    f1 = 2 * (p * r) / (p + r)

    return accuracy, cm, f1
