import torch.nn
from sklearn.metrics import confusion_matrix
import numpy as np


class BaselineModel(torch.nn.Module):
    def __init__(self, embeddings) -> None:
        super().__init__()
        self.embeddings = embeddings
        self.fc1 = torch.nn.Linear(300, 150, bias=True)
        self.fc2 = torch.nn.Linear(150, 150, bias=True)
        self.fc3 = torch.nn.Linear(150, 1, bias=True)


    def forward(self, x):
        dim = 1 if len(x.shape) == 2 else 0

        x = self.embeddings(x)
        x = torch.mean(x, dim=dim)

        x = torch.relu(self.fc1(x))
        x = torch.relu(self.fc2(x))
        x = self.fc3(x)
        return torch.reshape(x, (len(x),))


def train(model, data, optimizer, criterion):
    model.train()

    for batch_num, batch in enumerate(data):
        x = batch[0]
        y = batch[1]
        logits = model.forward(x)
        loss = criterion(logits, y.float())
        loss.backward()
        optimizer.step()
        optimizer.zero_grad()


def evaluate(model, data, criterion, args):
    model.eval()

    y_true = []
    y_pred = []
    losses = []

    with torch.no_grad():
        for batch_num, batch in enumerate(data):
            x = batch[0]
            y = batch[1].float()
            logits = model(x)
            pred = [1 if logit >= 0.5 else 0 for logit in logits]
            y_true.extend(y)
            y_pred.extend(pred)
            loss = criterion(logits, y)
            losses.append(loss)

    y_true = np.array(y_true)
    y_pred = np.array(y_pred)
    cm = confusion_matrix(y_true, y_pred)
    tn, fp, fn, tp = cm.ravel()

    accuracy = (tp + tn) / (tp + tn + fp + fn)
    p = (tp) / (tp + fp)
    r = (tp) / (tp + fn)
    f1 = 2 * (p * r) / (p + r)

    return accuracy, cm, f1
