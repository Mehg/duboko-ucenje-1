import torch
from torch.utils.data import DataLoader

from pathlib import Path

from RecurrentModel import train, evaluate, RecurrentModel
from Embeddings import get_frequencies, get_embeddings, apply_embeddings, pad_collate_fn
from NLPDataset import NLPDataset
from Vocab import Vocab

TRAIN_PATH = Path(__file__).parent / 'dataset' / 'sst_train_raw.csv'
TEST_PATH = Path(__file__).parent / 'dataset' / 'sst_test_raw.csv'
VALID_PATH = Path(__file__).parent / 'dataset' / 'sst_valid_raw.csv'
EMBED_PATH = Path(__file__).parent / 'dataset' / 'sst_glove_6b_300d.txt'
SAVE_PATH1 = Path(__file__).parent / 'results' / '4a'
SAVE_PATH2 = Path(__file__).parent / 'results' / '4b'
SAVE_PATH3 = Path(__file__).parent / 'results' / '4c'

train_batch = 10
test_batch = 32
lr = 1e-4
epochs = 5
rnn_cell_type = [torch.nn.RNN, torch.nn.GRU, torch.nn.LSTM]
rnn_cell_name = ["rnn", "gru", "lstm"]
gradient_clipping = 0.25
hidden_size = [150, 300, 280]
num_layers = [4, 5, 3]
dropout = [0.25, 0.5, 0.3]

min_freq = [1, 10, 100]
lrs = [1e-4, 1e-3, 1e-2]


def func4a():
    frequencies = get_frequencies(TRAIN_PATH)
    text_vocab = Vocab(frequencies, special={"<PAD>": 0, "<UNK>": 1})
    label_vocab = Vocab({}, special={"positive": 0, "negative": 1})
    embeddings = get_embeddings(text_vocab, EMBED_PATH)
    embeddings = apply_embeddings(embeddings)

    train_dataset, valid_dataset, test_dataset = NLPDataset(TRAIN_PATH, text_vocab, label_vocab), \
                                                 NLPDataset(VALID_PATH, text_vocab, label_vocab), \
                                                 NLPDataset(TEST_PATH, text_vocab, label_vocab)

    train_dataloader = DataLoader(dataset=train_dataset, batch_size=train_batch, shuffle=True,
                                  collate_fn=pad_collate_fn)
    valid_dataloader = DataLoader(dataset=valid_dataset, batch_size=test_batch, shuffle=False,
                                  collate_fn=pad_collate_fn)
    test_dataloader = DataLoader(dataset=test_dataset, batch_size=test_batch, shuffle=False,
                                 collate_fn=pad_collate_fn)

    for cell_type, cell_name in zip(rnn_cell_type, rnn_cell_name):
        for param_idx in range(3):
            model = RecurrentModel(embeddings, cell_type, hidden_size[param_idx], num_layers[param_idx],
                                   dropout[param_idx], False)

            criterion = torch.nn.BCEWithLogitsLoss()
            optimizer = torch.optim.Adam(model.parameters(), lr=1e-4)

            accs_valid = []
            cm_valid = []
            f1_valid = []

            for epoch in range(epochs):
                train(model, train_dataloader, optimizer, criterion, gradient_clipping)
                acc, cm, f1 = evaluate(model, valid_dataloader, criterion)
                accs_valid.append(acc)
                cm_valid.append(cm)
                f1_valid.append(f1)
                print(f'Epoch {epoch + 1}: valid accuracy = {acc}')

            acc, cm, f1 = evaluate(model, test_dataloader, criterion)
            print()
            print(f'Test accuracy = {acc}')

            with open(SAVE_PATH1 / f'{cell_name}-{param_idx}-not_bi.txt', 'w') as f:
                f.write("%s\n" % accs_valid)
                f.write("%s\n" % cm_valid)
                f.write("%s\n" % f1_valid)
                f.write("%s\n" % acc)
                f.write("%s\n" % cm)
                f.write("%s\n" % f1)


def func4b():
    frequencies = get_frequencies(TRAIN_PATH)
    label_vocab = Vocab({}, special={"positive": 0, "negative": 1})

    for param_idx in range(3):
        text_vocab = Vocab(frequencies, special={"<PAD>": 0, "<UNK>": 1}, min_freq=min_freq[param_idx])
        embeddings = get_embeddings(text_vocab, EMBED_PATH)
        embeddings = apply_embeddings(embeddings)

        train_dataset, valid_dataset, test_dataset = NLPDataset(TRAIN_PATH, text_vocab, label_vocab), \
                                                     NLPDataset(VALID_PATH, text_vocab, label_vocab), \
                                                     NLPDataset(TEST_PATH, text_vocab, label_vocab)

        train_dataloader = DataLoader(dataset=train_dataset, batch_size=train_batch, shuffle=True,
                                      collate_fn=pad_collate_fn)
        valid_dataloader = DataLoader(dataset=valid_dataset, batch_size=test_batch, shuffle=False,
                                      collate_fn=pad_collate_fn)
        test_dataloader = DataLoader(dataset=test_dataset, batch_size=test_batch, shuffle=False,
                                     collate_fn=pad_collate_fn)

        model = RecurrentModel(embeddings, rnn_cell_type[1], hidden_size[1], num_layers[1],
                               dropout[1], False)

        criterion = torch.nn.BCEWithLogitsLoss()
        optimizer = torch.optim.Adam(model.parameters(), lr=lrs[param_idx])

        accs_valid = []
        cm_valid = []
        f1_valid = []

        for epoch in range(epochs):
            train(model, train_dataloader, optimizer, criterion, gradient_clipping)
            acc, cm, f1 = evaluate(model, valid_dataloader, criterion)
            accs_valid.append(acc)
            cm_valid.append(cm)
            f1_valid.append(f1)
            print(f'Epoch {epoch + 1}: valid accuracy = {acc}')

        acc, cm, f1 = evaluate(model, test_dataloader, criterion)
        print()
        print(f'Test accuracy = {acc}')

        with open(SAVE_PATH2 / f'gru-{param_idx}-best.txt', 'w') as f:
            f.write("%s\n" % accs_valid)
            f.write("%s\n" % cm_valid)
            f.write("%s\n" % f1_valid)
            f.write("%s\n" % acc)
            f.write("%s\n" % cm)
            f.write("%s\n" % f1)

def func4c():
    frequencies = get_frequencies(TRAIN_PATH)
    label_vocab = Vocab({}, special={"positive": 0, "negative": 1})

    for param_idx in range(3):
        text_vocab = Vocab(frequencies, special={"<PAD>": 0, "<UNK>": 1}, min_freq=min_freq[param_idx])
        embeddings = get_embeddings(text_vocab, EMBED_PATH)
        embeddings = apply_embeddings(embeddings)

        train_dataset, valid_dataset, test_dataset = NLPDataset(TRAIN_PATH, text_vocab, label_vocab), \
                                                     NLPDataset(VALID_PATH, text_vocab, label_vocab), \
                                                     NLPDataset(TEST_PATH, text_vocab, label_vocab)

        train_dataloader = DataLoader(dataset=train_dataset, batch_size=train_batch, shuffle=True,
                                      collate_fn=pad_collate_fn)
        valid_dataloader = DataLoader(dataset=valid_dataset, batch_size=test_batch, shuffle=False,
                                      collate_fn=pad_collate_fn)
        test_dataloader = DataLoader(dataset=test_dataset, batch_size=test_batch, shuffle=False,
                                     collate_fn=pad_collate_fn)

        model = RecurrentModel(embeddings, rnn_cell_type[1], hidden_size[param_idx], num_layers[param_idx],
                               dropout[param_idx], False)

        criterion = torch.nn.BCEWithLogitsLoss()
        optimizer = torch.optim.Adam(model.parameters(), lr=lrs[param_idx])

        accs_valid = []
        cm_valid = []
        f1_valid = []

        for epoch in range(epochs):
            train(model, train_dataloader, optimizer, criterion, gradient_clipping)
            acc, cm, f1 = evaluate(model, valid_dataloader, criterion)
            accs_valid.append(acc)
            cm_valid.append(cm)
            f1_valid.append(f1)
            print(f'Epoch {epoch + 1}: valid accuracy = {acc}')

        acc, cm, f1 = evaluate(model, test_dataloader, criterion)
        print()
        print(f'Test accuracy = {acc}')

        with open(SAVE_PATH3 / f'gru-{param_idx}-best.txt', 'w') as f:
            f.write("%s\n" % accs_valid)
            f.write("%s\n" % cm_valid)
            f.write("%s\n" % f1_valid)
            f.write("%s\n" % acc)
            f.write("%s\n" % cm)
            f.write("%s\n" % f1)



# func4a()
func4b()
# func4c()