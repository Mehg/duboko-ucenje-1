import torch
import pandas as pd

from Vocab import Vocab
import numpy as np

dimension = 300


def get_embeddings(vocabulary: Vocab, file):
    embeddings = np.random.randn(len(vocabulary.stoi), dimension)
    embeddings[0] = np.zeros(dimension)  # <PAD> - 0

    if file is not None:
        with open(file) as f:
            lines = f.readlines()
            for line in lines:
                splitted = line.split(" ")
                word = splitted[0]
                encoded = vocabulary.encode(word)

                splitted.remove(word)

                embedding = np.array([float(x) for x in splitted])
                embeddings[encoded] = embedding

    return embeddings


def apply_embeddings(embeddings):
    return torch.nn.Embedding.from_pretrained(torch.Tensor(embeddings), padding_idx=0, freeze=False)


def get_frequencies(file):
    frequencies = dict()
    lines = pd.read_csv(file, header=None)
    for idx, line in lines.iterrows():
        for word in line[0].split(" "):
            value = frequencies.get(word, 0)
            frequencies[word] = value + 1

    return frequencies


def collate_fn(batch):
    """
    Arguments:
      Batch:
        list of Instances returned by `Dataset.__getitem__`.
    Returns:
      A tensor representing the input batch.
    """

    texts, labels = zip(*batch)  # Assuming the instance is in tuple-like form
    lengths = torch.Tensor([len(text) for text in texts])  # Needed for later
    # Process the text instances
    return texts, labels, lengths


def pad_collate_fn(batch, pad_index=0):
    texts, labels = zip(*batch)
    texts = torch.nn.utils.rnn.pad_sequence(texts, padding_value=pad_index, batch_first=True)
    labels = torch.tensor(labels)
    lengths = torch.tensor([len(text) for text in texts])
    return texts, labels, lengths
