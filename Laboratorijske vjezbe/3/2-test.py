import numpy as np
import torch
from torch.utils.data import DataLoader

from pathlib import Path

from BaselineModel import train, evaluate, BaselineModel
from Embeddings import get_frequencies, get_embeddings, apply_embeddings, pad_collate_fn
from NLPDataset import NLPDataset
from Vocab import Vocab

TRAIN_PATH = Path(__file__).parent / 'dataset' / 'sst_train_raw.csv'
TEST_PATH = Path(__file__).parent / 'dataset' / 'sst_test_raw.csv'
VALID_PATH = Path(__file__).parent / 'dataset' / 'sst_valid_raw.csv'
EMBED_PATH = Path(__file__).parent / 'dataset' / 'sst_glove_6b_300d.txt'
SAVE_PATH = Path(__file__).parent / 'results' / '2'

train_batch = 10
test_batch = 32
lr = 1e-4
epochs = 5

def func(s):
    np.random.seed(s)
    torch.manual_seed(s)

    frequencies = get_frequencies(TRAIN_PATH)
    text_vocab = Vocab(frequencies, special={"<PAD>": 0, "<UNK>": 1})
    label_vocab = Vocab({}, special={"positive": 0, "negative": 1})
    embeddings = get_embeddings(text_vocab, EMBED_PATH)
    embeddings = apply_embeddings(embeddings)

    train_dataset, valid_dataset, test_dataset = NLPDataset(TRAIN_PATH, text_vocab, label_vocab), \
                                                 NLPDataset(VALID_PATH, text_vocab, label_vocab), \
                                                 NLPDataset(TEST_PATH, text_vocab, label_vocab)

    train_dataloader = DataLoader(dataset=train_dataset, batch_size=train_batch, shuffle=True,
                                  collate_fn=pad_collate_fn)
    valid_dataloader = DataLoader(dataset=valid_dataset, batch_size=test_batch, shuffle=False,
                                  collate_fn=pad_collate_fn)
    test_dataloader = DataLoader(dataset=test_dataset, batch_size=test_batch, shuffle=False,
                                 collate_fn=pad_collate_fn)

    model = BaselineModel(embeddings)

    criterion = torch.nn.BCEWithLogitsLoss()
    optimizer = torch.optim.Adam(model.parameters(), lr=1e-4)

    accs_valid = []
    cm_valid = []
    f1_valid = []

    for epoch in range(epochs):
        train(model, train_dataloader, optimizer, criterion)
        acc, cm, f1 = evaluate(model, valid_dataloader, criterion, None)
        accs_valid.append(acc)
        cm_valid.append(cm)
        f1_valid.append(f1)
        print(f'Epoch {epoch + 1}: valid accuracy = {acc}')

    acc, cm, f1 = evaluate(model, test_dataloader, criterion, None)
    print()
    print(f'Test accuracy = {acc}')

    with open(SAVE_PATH / f'{s}.txt', 'w') as f:
        f.write("%s\n" % accs_valid)
        f.write("%s\n" % cm_valid)
        f.write("%s\n" % f1_valid)
        f.write("%s\n" % acc)
        f.write("%s\n" % cm)
        f.write("%s\n" % f1)


for _ in range(5):
    seed = np.random.randint(1, 2 ** 32, dtype='int64')
    func(seed)
