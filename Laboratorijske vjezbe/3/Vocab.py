import torch
from typing import Dict


class Vocab:
    def __init__(self, frequencies: Dict[str, int], special: Dict[str, int], max_size=-1, min_freq=1):
        frequencies = {key: value for (key, value) in frequencies.items() if value >= min_freq}
        frequencies = {key: value for (key, value) in sorted(frequencies.items(), key=lambda item: item[1], reverse=True)}
        if max_size != -1:
            frequencies = frequencies[: max_size]

        itos = dict()
        stoi = dict()

        for (key, value) in special.items():
            itos[value] = key
            stoi[key] = value

        offset = len(special)
        for idx, (key, value) in enumerate(frequencies.items()):
            itos[idx + offset] = key
            stoi[key] = idx + offset

        self.itos = itos
        self.stoi = stoi
        self.max_size = max_size
        self.min_freq = min_freq

    def encode(self, text) -> torch.Tensor:
        if isinstance(text, list):
            return torch.tensor([self.stoi.get(x, 1) for x in text])
        return self.stoi.get(text, 1)

    def decode(self, num):
        if isinstance(num, list):
            return torch.tensor([self.itos.get(x, self.itos.get(1)) for x in num])
        return self.itos.get(num, self.itos.get(1))
