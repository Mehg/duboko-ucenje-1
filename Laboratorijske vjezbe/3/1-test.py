from torch.utils.data import DataLoader
from pathlib import Path

from NLPDataset import NLPDataset
from Vocab import Vocab
from Embeddings import get_frequencies, get_embeddings, pad_collate_fn

TRAIN_PATH = Path(__file__).parent / 'dataset' / 'sst_train_raw.csv'
EMBED_PATH = Path(__file__).parent / 'dataset' / 'sst_glove_6b_300d.txt'

frequencies = get_frequencies(TRAIN_PATH)
text_vocab = Vocab(frequencies, special={"<PAD>": 0, "<UNK>": 1})
label_vocab = Vocab({}, special={"positive": 0, "negative": 1})
train_dataset = NLPDataset(TRAIN_PATH, text_vocab, label_vocab)

instance_text, instance_label = train_dataset.instances[3]
# Referenciramo atribut klase pa se ne zove nadjačana metoda
print(f"Text: {instance_text}")
print(f"Label: {instance_label}")
# >>> Text: ['yet', 'the', 'act', 'is', 'still', 'charming', 'here']
# >>> Label: positive
numericalized_text, numericalized_label = train_dataset[3]
# Koristimo nadjačanu metodu indeksiranja
print(f"Numericalized text: {numericalized_text}")
print(f"Numericalized label: {numericalized_label}")
# >>> Numericalized text: tensor([189,   2, 674,   7, 129, 348, 143])
# >>> Numericalized label: tensor(0)

print()

embeddings = get_embeddings(text_vocab, EMBED_PATH)
print(embeddings.shape)
print(embeddings[text_vocab.stoi["the"]])

print()
batch_size = 2  # Only for demonstrative purposes
shuffle = False  # Only for demonstrative purposes
train_dataloader = DataLoader(dataset=train_dataset, batch_size=batch_size,
                              shuffle=shuffle, collate_fn=pad_collate_fn)
texts, labels, lengths = next(iter(train_dataloader))
print(f"Texts: {texts}")
print(f"Labels: {labels}")
print(f"Lengths: {lengths}")

# >>> Texts: tensor([[   2,  554,    7, 2872,    6,   22,    2, 2873, 1236,    8,   96, 4800,
#                        4,   10,   72,    8,  242,    6,   75,    3, 3576,   56, 3577,   34,
#                     2022, 2874, 7123, 3578, 7124,   42,  779, 7125,    0,    0],
#                    [   2, 2875, 2023, 4801,    5,    2, 3579,    5,    2, 2876, 4802,    7,
#                       40,  829,   10,    3, 4803,    5,  627,   62,   27, 2877, 2024, 4804,
#                      962,  715,    8, 7126,  555,    5, 7127, 4805,    8, 7128]])
# >>> Labels: tensor([0, 0])
# >>> Lengths: tensor([32, 34])
