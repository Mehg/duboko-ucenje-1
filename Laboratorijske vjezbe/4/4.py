import numpy as np
import torch
import os
from pathlib import Path

from dataset import MNISTMetricDataset
from model import SimpleMetricEmbedding
from matplotlib import pyplot as plt

MODEL_PATH = Path(os.path.abspath('')) / 'models' / 'model.pth'
MODEL_PATH_NO_ZERO = Path(os.path.abspath('')) / 'models' / 'modelNoZero.pth'


def get_colormap():
    # Cityscapes colormap for first 10 classes
    colormap = np.zeros((10, 3), dtype=np.uint8)
    colormap[0] = [128, 64, 128]
    colormap[1] = [244, 35, 232]
    colormap[2] = [70, 70, 70]
    colormap[3] = [102, 102, 156]
    colormap[4] = [190, 153, 153]
    colormap[5] = [153, 153, 153]
    colormap[6] = [250, 170, 30]
    colormap[7] = [220, 220, 0]
    colormap[8] = [107, 142, 35]
    colormap[9] = [152, 251, 152]
    return colormap


if __name__ == '__main__':
    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    print(f"= Using device {device}")
    emb_size = 32

    model_no_zero = SimpleMetricEmbedding(1, emb_size)
    model_no_zero.load_state_dict(torch.load(MODEL_PATH_NO_ZERO))
    model_no_zero.to(device)

    model = SimpleMetricEmbedding(1, emb_size)
    model.load_state_dict(torch.load(MODEL_PATH))
    model.to(device)

    colormap = get_colormap()
    colorx = np.arange(0, 10)
    plt.scatter(colorx, colorx, color=colormap[colorx[:]] / 255)
    plt.show()

    mnist_download_root = "./mnist/"
    ds_test = MNISTMetricDataset(mnist_download_root, split='test')
    X = ds_test.images
    Y = ds_test.targets
    plt.title("Fitting PCA directly from images...")
    test_img_rep2d = torch.pca_lowrank(ds_test.images.view(-1, 28 * 28), 2)[0]
    plt.scatter(test_img_rep2d[:, 0], test_img_rep2d[:, 1], color=colormap[Y[:]] / 255., s=5)
    plt.show()
    plt.figure()

    with torch.no_grad():
        model.eval()
        test_rep = model.get_features(X.unsqueeze(1))
        test_rep2d = torch.pca_lowrank(test_rep, 2)[0]
        plt.title("Fitting PCA from feature representation - basic model")
        plt.scatter(test_rep2d[:, 0], test_rep2d[:, 1], color=colormap[Y[:]] / 255., s=5)
        plt.show()

    with torch.no_grad():
        model_no_zero.eval()
        test_rep = model_no_zero.get_features(X.unsqueeze(1))
        test_rep2d = torch.pca_lowrank(test_rep, 2)[0]
        plt.title("Fitting PCA from feature representation - model no zero")
        plt.scatter(test_rep2d[:, 0], test_rep2d[:, 1], color=colormap[Y[:]] / 255., s=5)
        plt.show()
