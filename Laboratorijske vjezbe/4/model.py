import torch
import torch.nn as nn


class _BNReluConv(nn.Sequential):
    def __init__(self, num_maps_in, num_maps_out, k=3, bias=True):
        super(_BNReluConv, self).__init__()
        self.append(torch.nn.BatchNorm2d(num_maps_in))
        self.append(torch.nn.Conv2d(in_channels=num_maps_in, out_channels=num_maps_out, kernel_size=k, bias=bias))
        self.append(torch.nn.ReLU())


class SimpleMetricEmbedding(nn.Module):
    def __init__(self, input_channels, emb_size=32, margin=1, distance=torch.nn.PairwiseDistance):
        super().__init__()
        self.emb_size = emb_size
        self.margin = margin
        self.distance = distance()

        self.bn1 = _BNReluConv(input_channels, emb_size)
        self.mp1 = torch.nn.MaxPool2d(3, 2)
        self.bn2 = _BNReluConv(emb_size, emb_size)
        self.mp2 = torch.nn.MaxPool2d(3, 2)
        self.bn3 = _BNReluConv(emb_size, emb_size)
        self.avg = torch.nn.AvgPool2d(2)

    def get_features(self, img):
        # Returns tensor with dimensions BATCH_SIZE, EMB_SIZE
        # YOUR CODE HERE

        batch_size = img.shape[0]
        x = self.bn1(img)
        x = self.mp1(x)
        x = self.bn2(x)
        x = self.mp2(x)
        x = self.bn3(x)
        x = self.avg(x)

        return x.reshape(batch_size, self.emb_size)

    def loss(self, anchor, positive, negative):
        a_x = self.get_features(anchor)
        p_x = self.get_features(positive)
        n_x = self.get_features(negative)

        dap = self.distance(a_x, p_x)
        dan = self.distance(a_x, n_x)
        loss = torch.mean(torch.relu(dap - dan + self.margin))
        return loss


class IdentityModel(nn.Module):
    def __init__(self):
        super(IdentityModel, self).__init__()

    def get_features(self, img):
        feats = img.reshape(img.shape[0], img.shape[1] * img.shape[2] * img.shape[3])
        return feats
