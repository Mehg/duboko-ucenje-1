import time
import os
import torch.optim
from pathlib import Path
from dataset import MNISTMetricDataset
from torch.utils.data import DataLoader
from model import SimpleMetricEmbedding, IdentityModel
from utils import train, evaluate, compute_representations

EVAL_ON_TEST = True
EVAL_ON_TRAIN = False

MODEL_PATH = Path(os.path.abspath('')) / 'models' / 'model.pth'
MODEL_PATH_NO_ZERO = Path(os.path.abspath('')) / 'models' / 'modelNoZero.pth'

if __name__ == '__main__':
    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    print(f"= Using device {device}")

    # CHANGE ACCORDING TO YOUR PREFERENCE
    mnist_download_root = "./mnist/"
    ds_train = MNISTMetricDataset(mnist_download_root, split='train')
    ds_train_no_zero = MNISTMetricDataset(mnist_download_root, split='train', remove_class=0)
    ds_test = MNISTMetricDataset(mnist_download_root, split='test')
    ds_traineval = MNISTMetricDataset(mnist_download_root, split='traineval')

    num_classes = 10

    print(f"> Loaded {len(ds_train)} training images!")
    print(f"> Loaded {len(ds_test)} validation images!")

    train_loader = DataLoader(
        ds_train,
        batch_size=64,
        shuffle=True,
        pin_memory=True,
        num_workers=4,
        drop_last=True
    )

    train_loader_no_zero = DataLoader(
        ds_train_no_zero,
        batch_size=64,
        shuffle=True,
        pin_memory=True,
        num_workers=4,
        drop_last=True
    )

    test_loader = DataLoader(
        ds_test,
        batch_size=1,
        shuffle=False,
        pin_memory=True,
        num_workers=1
    )

    traineval_loader = DataLoader(
        ds_traineval,
        batch_size=1,
        shuffle=False,
        pin_memory=True,
        num_workers=1
    )

    emb_size = 32
    model = SimpleMetricEmbedding(1, emb_size).to(device)
    identity = IdentityModel().to(device)
    optimizer = torch.optim.Adam(
        model.parameters(),
        lr=1e-3
    )

    epochs = 3
    for epoch in range(epochs):
        print(f"Epoch: {epoch}")
        t0 = time.time_ns()
        train_loss = train(model, optimizer, train_loader, device)
        # train_loss = train(model, optimizer, train_loader_no_zero, device)
        print(f"Mean Loss in Epoch {epoch}: {train_loss:.3f}")
        if EVAL_ON_TEST or EVAL_ON_TRAIN:
            print("Computing mean representations for evaluation...")
            representations = compute_representations(model, train_loader, num_classes, emb_size, device)
        if EVAL_ON_TRAIN:
            print("Evaluating on training set...")
            acc1 = evaluate(model, representations, traineval_loader, device)
            print(f"Epoch {epoch}: Train Top1 Acc: {round(acc1 * 100, 2)}%")
        if EVAL_ON_TEST:
            print("Evaluating on test set...")
            acc1 = evaluate(model, representations, test_loader, device)
            print(f"Epoch {epoch}: Test Accuracy: {acc1 * 100:.2f}%")
        t1 = time.time_ns()
        print(f"Epoch time (sec): {(t1 - t0) / 10 ** 9:.1f}")

    torch.save(model.state_dict(), MODEL_PATH)

# a) Analiza modula utils.py
# _____________________________________________________________
# Kako se računaju reprezentacije razreda?
# za svaki batch racuna se get_features koji vraca [BATCH_SIZE, EMBED_SIZE] matricu.
# Reprezentacije se spremaju ovisno o razredu te se racuna srednja reprezentacija za svaki razred
# Kako se provodi klasifikacija primjera?
# Za dani primjer racuna se get_features te se gleda L2 udaljenost od srednje reprezentacije svakog razreda
# Kao zavrsna predikcija uzima se onaj razred koji je najblizi
# Probajte smisliti alternativne pristupe za klasifikaciju primjera.
# Mogli bi imati drugu udaljenost?


# b) Klasifikacija na temelju metričkog ugrađivanja
# _____________________________________________________________
# Epoch: 0
# Mean Loss in Epoch 0: 0.051
# Epoch 0: Test Accuracy: 97.93%
# Epoch time (sec): 134.6
# Epoch: 1
# Mean Loss in Epoch 1: 0.022
# Epoch 1: Test Accuracy: 98.10%
# Epoch time (sec): 156.0
# Epoch: 2
# Mean Loss in Epoch 2: 0.016
# Epoch 2: Test Accuracy: 98.25%
# Epoch time (sec): 119.4

# c) Klasifikacija na temelju udaljenosti u prostoru slike
# _____________________________________________________________
# Epoch: 0
# Mean Loss in Epoch 0: 0.053
# Epoch 0: Test Accuracy: 82.15%
# Epoch time (sec): 260.1
# Epoch: 1
# Mean Loss in Epoch 1: 0.022
# Epoch 1: Test Accuracy: 82.16%
# Epoch time (sec): 317.5
# Epoch: 2
# Mean Loss in Epoch 2: 0.017
# Epoch 2: Test Accuracy: 82.15%
# Epoch time (sec): 356.8

# e) Klasifikacija neviđenih razreda
# _____________________________________________________________
# Epoch: 0
# Mean Loss in Epoch 0: 0.054
# Epoch 0: Test Accuracy: 95.49%
# Epoch time (sec): 108.9
# Epoch: 1
# Mean Loss in Epoch 1: 0.024
# Epoch 1: Test Accuracy: 96.92%
# Epoch time (sec): 123.8
# Epoch: 2
# Mean Loss in Epoch 2: 0.018
# Epoch 2: Test Accuracy: 96.95%
# Epoch time (sec): 104.8
