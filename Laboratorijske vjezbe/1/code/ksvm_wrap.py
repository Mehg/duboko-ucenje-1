import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
import matplotlib.pyplot as plt
import sklearn.svm as svm
import data


class KSVMWrap():
    def __init__(self, X, Y_, param_svm_c=1, param_svm_gamma='auto'):
        """Konstruira omotač i uči RBF SVM klasifikator
        X, Y_:           podatci i točni indeksi razreda
        param_svm_c:     relativni značaj podatkovne cijene
        param_svm_gamma: širina RBF jezgre"""
        super().__init__()
        self.model = svm.SVC(C=param_svm_c, gamma=param_svm_gamma, kernel='rbf', probability=True)
        self.model.fit(X, Y_)

    def predict(self, X):
        """Predviđa i vraća indekse razreda podataka X"""
        return self.model.predict(X)

    def get_scores(self, X):
        """Vraća klasifikacijske mjere
        (engl. classification scores) podataka X;
        ovo će vam trebati za računanje prosječne preciznosti."""
        return self.model.predict_proba(X)

    def support(self):
        """Indeksi podataka koji su odabrani za potporne vektore"""
        return self.model.support_


if __name__ == "__main__":
    np.random.seed(100)

    X, Y_ = data.sample_gmm_2d(6, 2, 10)
    ksvm = KSVMWrap(X, Y_)

    probs = ksvm.get_scores(X)
    Y = ksvm.predict(X)

    acc, p, r = data.eval_perf_binary(Y, Y_)
    ap = data.eval_AP(Y_[probs[:, 1].argsort()])
    print()
    print(f"Performance: acc={acc}, p={p}, r={r}, ap={ap}")

    # decfun = deep_decfun(ptd)
    bbox = (np.min(X, axis=0), np.max(X, axis=0))
    data.graph_surface(lambda x: ksvm.get_scores(x)[:, 1], bbox, offset=0.5)

    # graph the data points
    data.graph_data(X, Y_, Y, special=ksvm.support())
    plt.show()
