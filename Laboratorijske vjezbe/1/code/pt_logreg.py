import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
import matplotlib.pyplot as plt

import data


class PTLogreg(nn.Module):
    def __init__(self, D, C):
        """Arguments:
           - D: dimensions of each datapoint
           - C: number of classes
        """
        super().__init__()
        self.W = nn.Parameter(torch.randn((D, C)), requires_grad=True)
        self.b = nn.Parameter(torch.zeros((C)), requires_grad=True)

    def forward(self, X):
        s = X.mm(self.W) + self.b
        out = torch.softmax(s, dim=1)
        return out

    def get_loss(self, X, Yoh_):
        out = self.forward(X)
        correct_y = out[np.arange(len(out)), Yoh_.argmax(axis=1)]
        log_out = torch.log(correct_y)
        loss = -torch.mean(log_out)
        return loss


def train(model, X, Yoh_, param_niter, param_delta, param_lambda):
    """Arguments:
       - X: model inputs [NxD], type: torch.Tensor
       - Yoh_: ground truth [NxC], type: torch.Tensor
       - param_niter: number of training iterations
       - param_delta: learning rate
    """
    optimizer = optim.SGD(params=model.parameters(), lr=param_delta)

    for i in range(param_niter):
        loss = model.get_loss(X, Yoh_) + param_lambda/2 * torch.norm(model.W)**2
        loss.backward()
        optimizer.step()

        print(f'step: {i}, loss:{loss}, W:{model.W}, b {model.b}')

        optimizer.zero_grad()


def eval(model, X):
    """Arguments:
       - model: type: PTLogreg
       - X: actual datapoints [NxD], type: np.array
       Returns: predicted class probabilites [NxC], type: np.array
    """
    return model.forward(torch.tensor(X, dtype=torch.float)).detach().numpy()


def logreg_decfun(model):
    def classify(X):
        return np.argmax(eval(model, X), axis=1)

    return classify


if __name__ == "__main__":
    # inicijaliziraj generatore slučajnih brojeva
    np.random.seed(100)

    # instanciraj podatke X i labele Yoh_
    x, Y_ = data.sample_gmm_2d(4, 3, 20)
    X = torch.tensor(x, dtype=torch.float)
    Yoh_ = torch.tensor(data.class_to_onehot(Y_), dtype=torch.float)

    # definiraj model:
    ptlr = PTLogreg(X.shape[1], Yoh_.shape[1])

    # nauči parametre (X i Yoh_ moraju biti tipa torch.Tensor):
    train(ptlr, X, Yoh_, 1000, 0.1, 0)

    # dohvati vjerojatnosti na skupu za učenje
    probs = eval(ptlr, x)
    Y = np.argmax(probs, axis=1)

    # ispiši performansu (preciznost i odziv po razredima)
    accuracy, pr, M = data.eval_perf_multi(Y, Y_)
    print()
    print(f"Performance: acc={accuracy}, pr={pr}, M={M}")

    # iscrtaj rezultate, decizijsku plohu
    decfun = logreg_decfun(ptlr)
    bbox = (np.min(x, axis=0), np.max(x, axis=0))
    data.graph_surface(decfun, bbox, offset=0.5)

    # graph the data points
    data.graph_data(x, Y_, Y, special=[])
    plt.show()
