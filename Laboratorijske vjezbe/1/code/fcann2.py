import numpy as np
import matplotlib.pyplot as plt
import data

param_niter = 100000
param_delta = 0.05
param_lambda = 0.0001


def ReLu(x):
    return np.maximum(0, x)


def softmax(x):
    exps2 = np.exp(x)
    sumexp = np.sum(exps2, axis=1)
    return exps2 / sumexp[:, None]


def calculate_loss(Y_, probs):
    correct_y = probs[np.arange(len(probs)), Y_.argmax(axis=1)]
    return -np.mean(np.log(correct_y))


def fcann2_train(X, Y_, hidden):
    N = len(X)
    C = len(Y_[0])
    D = len(X[0])

    W1 = np.random.randn(D, hidden)
    b1 = np.zeros((1, hidden))
    W2 = np.random.randn(hidden, C)
    b2 = np.zeros((1, C))

    for i in range(param_niter):
        s1 = np.matmul(X, W1) + b1
        h1 = ReLu(s1)
        s2 = np.matmul(h1, W2) + b2
        probs = softmax(s2)

        loss = calculate_loss(Y_, probs)
        squared_weight_parts = np.linalg.norm(W1)**2+np.linalg.norm(W2)**2
        loss += param_lambda * squared_weight_parts

        G_s2 = probs
        G_s2[np.arange(len(probs)), np.argmax(Y_, axis=1)] -= 1
        G_s2 = G_s2 / N

        # dijagnostički ispis
        if i % 10 == 0:
            print("iteration {}: loss {}".format(i, loss))

        grad_W2 =np.matmul(np.transpose(h1), G_s2)  # CxH
        grad_b2 =np.transpose(np.sum(G_s2, axis=0))  # Cx1

        G_s1 = np.matmul(G_s2, np.transpose(W2))
        G_s1[h1 <= float(0)] = 0

        grad_W1 = np.matmul(np.transpose(X), G_s1)
        grad_b1 = np.transpose(np.sum(G_s1, axis=0))

        W1 += -param_delta * grad_W1
        b1 += -param_delta * grad_b1
        W2 += -param_delta * grad_W2
        b2 += -param_delta * grad_b2

    return W1, b1, W2, b2


def fcann2_classify(X, W1, b1, W2, b2):
    s1 = np.matmul(X, W1) + b1
    h1 = ReLu(s1)
    s2 = np.matmul(h1, W2) + b2
    return softmax(s2)


def fcann2_decfun(W1, b1, W2, b2):
    def classify(X):
        probs = fcann2_classify(X, W1, b1, W2, b2)
        return probs[:, 1]

    return classify


if __name__ == "__main__":
    np.random.seed(100)

    # get the training dataset
    X, Y_ = data.sample_gmm_2d(6, 2, 10)

    # train the model
    w1, b1, w2, b2 = fcann2_train(X, data.class_to_onehot(Y_), 5)

    # evaluate the model on the training dataset
    probs = fcann2_classify(X, w1, b1, w2, b2)
    Y = np.array([np.argmax(row) for row in probs])

    # report performance
    accuracy, recall, precision = data.eval_perf_binary(Y, Y_)

    print(accuracy, recall, precision)

    # graph the decision surface
    decfun = fcann2_decfun(w1, b1, w2, b2)
    bbox = (np.min(X, axis=0), np.max(X, axis=0))
    data.graph_surface(decfun, bbox, offset=0.5)

    # graph the data points
    data.graph_data(X, Y_, Y, special=[])
    plt.show()
