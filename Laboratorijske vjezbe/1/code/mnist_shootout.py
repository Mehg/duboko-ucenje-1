import torchvision
import torch.optim as optim
import pt_deep
import data
import numpy as np
import torch
import matplotlib.pyplot as plt

dataset_root = '/tmp/mnist'  # change this to your preference
mnist_train = torchvision.datasets.MNIST(dataset_root, train=True, download=True)
mnist_test = torchvision.datasets.MNIST(dataset_root, train=False, download=True)

x_train, y_train = mnist_train.data, mnist_train.targets
x_test, y_test = mnist_test.data, mnist_test.targets
x_train, x_test = x_train.float().div_(255.0), x_test.float().div_(255.0)

N = x_train.shape[0]
D = x_train.shape[1] * x_train.shape[2]
C = y_train.max().add_(1).item()

x_train = x_train.reshape((N, D))
x_test = x_test.reshape((len(x_test), D))

def train(model, X, Yoh_, param_niter, param_delta, param_lambda, print_bool=True):
    optimizer = optim.SGD(params=model.parameters(), lr=param_delta)

    for i in range(param_niter):
        loss = model.get_loss(X, Yoh_)
        loss += param_lambda / 2 * torch.mean(torch.tensor([torch.norm(wi) ** 2 for wi in model.weights]))
        loss.backward()
        optimizer.step()

        if print_bool:
            print(f'step: {i}, loss:{loss}, W:{model.weights}, b {model.biases}')

        optimizer.zero_grad()


if __name__ == "__main__":
    np.random.seed(100)

    Yoh_ = torch.tensor(data.class_to_onehot(y_train), dtype=torch.float)

    ptd = pt_deep.PTDeep([D, C], torch.relu)
    train(ptd, x_train, Yoh_, 3000, 0.1, 0, print_bool=True)

    probs = pt_deep.eval(ptd, x_test)
    Y = np.argmax(probs, axis=1)

    accuracy, pr, M = data.eval_perf_multi(Y, y_test)
    print()
    print(f"Performance: acc={accuracy},\n pr={pr},\n M={M}")

    weights = ptd.weights[0].detach().numpy().T

    for i, wi in enumerate(weights):
        plt.imshow(wi.reshape(28, 28))
        plt.colorbar()
        plt.show()
