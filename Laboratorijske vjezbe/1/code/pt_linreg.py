import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
import matplotlib.pyplot as plt

## Definicija računskog grafa
# podaci i parametri, inicijalizacija parametara
a = torch.randn(1, requires_grad=True)
b = torch.randn(1, requires_grad=True)

X = torch.tensor([1, 2, 3])
Y = torch.tensor([3, 4, 8])

# optimizacijski postupak: gradijentni spust
optimizer = optim.SGD([a, b], lr=0.1)


def gradient(a, b):
    N = len(X)
    b_grad = 2 * (a * X + b - Y)
    a_grad = b_grad * X
    b_grad = torch.sum(b_grad) / N
    a_grad = torch.sum(a_grad) / N
    return a_grad, b_grad


for i in range(100):
    # afin regresijski model
    Y_ = a * X + b

    diff = (Y - Y_)

    # kvadratni gubitak
    loss = 1 / len(X) * torch.sum(diff ** 2)

    # računanje gradijenata
    loss.backward()

    print(f'grad torch: {a.grad}, {b.grad}')
    a_grad, b_grad = gradient(a, b)
    print(f'grad calc: {a_grad}, {b_grad}')

    # korak optimizacije
    optimizer.step()

    # Postavljanje gradijenata na nulu
    optimizer.zero_grad()

    print(f'step: {i}, loss:{loss}, Y_:{Y_}, a:{a}, b {b}')

plt.scatter(X.numpy(), Y.numpy())
linspace = np.linspace(1, 4)
plt.plot(linspace, a.detach().numpy() * linspace + b.detach().numpy())
plt.show()
