import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
import matplotlib.pyplot as plt

import data


class PTDeep(nn.Module):
    def __init__(self, layers: [], activation_function):
        """Arguments:
           - layers: dimensions of each layer
           - activation_function: activation function of hidden layers
        """
        super().__init__()
        weights = []
        biases = []

        for i in range(0, len(layers) - 1, 1):
            Wi = nn.Parameter(torch.randn((layers[i], layers[i + 1])), requires_grad=True)
            bi = nn.Parameter(torch.zeros((1, layers[i + 1])), requires_grad=True)
            weights.append(Wi)
            biases.append(bi)

        self.activation_function = activation_function
        self.weights = nn.ParameterList(weights)
        self.biases = nn.ParameterList(biases)

    def forward(self, X):
        s = X
        for wi, bi in zip(self.weights, self.biases):
            s = X.mm(wi) + bi
            X = self.activation_function(s)

        out = torch.softmax(s, dim=1)
        return out

    def get_loss(self, X, Yoh_):
        out = self.forward(X)
        correct_y = out[np.arange(len(out)), Yoh_.argmax(axis=1)]
        log_out = torch.log(correct_y)
        loss = -torch.mean(log_out)
        return loss


def train(model, X, Yoh_, param_niter, param_delta, param_lambda, print_bool=True):
    """Arguments:
       - X: model inputs [NxD], type: torch.Tensor
       - Yoh_: ground truth [NxC], type: torch.Tensor
       - param_niter: number of training iterations
       - param_delta: learning rate
    """
    optimizer = optim.SGD(params=model.parameters(), lr=param_delta)

    for i in range(param_niter):
        loss = model.get_loss(X, Yoh_) + param_lambda * torch.sum(
            torch.tensor([torch.norm(wi) ** 2 for wi in model.weights]))
        loss.backward()
        optimizer.step()

        if print_bool:
            print(f'step: {i}, loss:{loss}, W:{model.weights}, b {model.biases}')

        optimizer.zero_grad()


def eval(model, X):
    """Arguments:
       - model: type: PTLogreg
       - X: actual datapoints [NxD], type: np.array
       Returns: predicted class probabilites [NxC], type: np.array
    """
    return model.forward(torch.tensor(X, dtype=torch.float)).detach().numpy()


def count_params(model: nn.Module):
    count = 0
    print("Parameters: ")
    for param in model.named_parameters():
        print(f'    name: {param[0]}, dimension: {param[1].shape} ')
        count += len(param[1]) * len(param[1][0])
    print(f'    total number of parameters: {count}')


def deep_decfun(model):
    def classify(X):
        return eval(model, X)[:, 1]

    return classify


if __name__ == "__main__":
    # inicijaliziraj generatore slučajnih brojeva
    np.random.seed(100)

    # instanciraj podatke X i labele Yoh_
    x, Y_ = data.sample_gmm_2d(6, 2, 10)
    X = torch.tensor(x, dtype=torch.float)
    Yoh_ = torch.tensor(data.class_to_onehot(Y_), dtype=torch.float)

    # definiraj model:
    ptd = PTDeep([2, 5, 2], torch.relu)

    # nauči parametre (X i Yoh_ moraju biti tipa torch.Tensor):
    train(ptd, X, Yoh_, 100000, 0.05, 0.0001, False)

    # dohvati vjerojatnosti na skupu za učenje
    probs = eval(ptd, x)
    Y = np.argmax(probs, axis=1)

    # ispiši performansu (preciznost i odziv po razredima)
    acc, p, r = data.eval_perf_binary(Y, Y_)
    ap = data.eval_AP(Y_[probs[:, 1].argsort()])
    print()
    print(f"Performance: acc={acc}, p={p}, r={r}, ap={ap}")
    count_params(ptd)

    # iscrtaj rezultate, decizijsku plohu
    decfun = deep_decfun(ptd)
    bbox = (np.min(x, axis=0), np.max(x, axis=0))
    data.graph_surface(decfun, bbox, offset=0.5)

    # graph the data points
    data.graph_data(x, Y_, Y, special=[])
    plt.show()
